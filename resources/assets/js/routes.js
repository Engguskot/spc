// login
import Login	from './components/views/login/Form';
// Panel
import Panel	from './components/views/panel/Home';
import DashBoard	from './components/views/panel/DashBoard';
//Administracion de ayuda
import AdminHelp from './components/views/admin/help/AdminHelp';

export const routes = [
	{
		path: '/ingresar',
		component: Login,
		beforeEnter(to, from, next) {
			if( localStorage.getItem('gmm_token') ) {
				next('/');
			} else {
				next();
			}
		}
	},
	{
		path: '/',
		component: Panel,
		children: [
			{
				path: '',
				component: DashBoard
			},
			{
				path: 'admin/ayuda',
				component: AdminHelp
			}
		],
		beforeEnter(to, from, next) {
			if ( localStorage.getItem('gmm_token') ) {
				next();
			} else {
				next('/ingresar');
			}
		},
	}
];
