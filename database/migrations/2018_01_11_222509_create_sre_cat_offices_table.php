<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSreCatOfficesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sre_cat_offices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('shortName')->nullable();
            $table->string('longName')->nullable();
            $table->string('officeSiarIdentifier')->nullable();
            $table->integer('sre_cat_office_type_id')->unsigned();
            $table->string('streetName')->nullable();
            $table->string('buldingNum')->nullable();
            $table->string('apartmentNum')->nullable();
            $table->string('neighborhood')->nullable();
            $table->integer('geo_cat_country_id')->unsigned()->nullable();
            $table->integer('geo_cat_state_id')->unsigned()->nullable();
            $table->integer('geo_cat_municipality_id')->unsigned()->nullable();
            $table->string('postalCode')->nullable();
            $table->string('telephone')->nullable();
            $table->string('fax')->nullable();
            $table->string('email')->nullable();
            $table->string('references')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('sre_cat_office_type_id')
                  ->references('id')
                  ->on('sre_cat_office_types');

            $table->foreign('geo_cat_country_id')
                  ->references('id')
                  ->on('geo_cat_countries');

            $table->foreign('geo_cat_state_id')
                  ->references('id')
                  ->on('geo_cat_states');

            $table->foreign('geo_cat_municipality_id')
                  ->references('id')
                  ->on('geo_cat_municipalities');

            $table->index([
                        'shortName',
                        'longName',
                        'officeSiarIdentifier'
                    ],'sre_cat_offices_column_index');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
