<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSreFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sre_files', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fileName');
            $table->string('fileNameHash');
            $table->integer('fileStatus')->unsigned()->default('0');
            $table->timestamps();
            $table->softDeletes();

            $table->index([
                        'fileName',
                        'fileNameHash',
                        'fileStatus'
                    ],'sre_files_column_index');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
