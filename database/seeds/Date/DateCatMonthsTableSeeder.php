<?php

use Illuminate\Database\Seeder;

class DateCatMonthsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('date_cat_months')->insert( [
		    [ 'id' => 1,  'name' => 'Enero',		'shortName' => 'Ene'],
		    [ 'id' => 2,  'name' => 'Febrero',		'shortName' => 'Feb'],
		    [ 'id' => 3,  'name' => 'Marzo',		'shortName' => 'Mar'],
		    [ 'id' => 4,  'name' => 'Abril',		'shortName' => 'Abr'],
		    [ 'id' => 5,  'name' => 'Mayo',			'shortName' => 'May'],
		    [ 'id' => 6,  'name' => 'Junio',		'shortName' => 'Jun'],
		    [ 'id' => 7,  'name' => 'Julio',		'shortName' => 'Jul'],
		    [ 'id' => 8,  'name' => 'Agosto',		'shortName' => 'Ago'],
		    [ 'id' => 9,  'name' => 'Septiembre',	'shortName' => 'Sep'],
		    [ 'id' => 10, 'name' => 'Octubre',		'shortName' => 'Oct'],
		    [ 'id' => 11, 'name' => 'Noviembre',	'shortName' => 'Nov'],
		    [ 'id' => 12, 'name' => 'Diciembre',	'shortName' => 'Dic']
	    ]);
    }
}
