<?php

use Illuminate\Database\Seeder;

class SreCatEmployeeSeparationTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('sre_cat_employee_separation_types')->insert(
	    	[
			    [ 'name' => 'Renuncia'],
			    [ 'name' => 'Abandono de empleo'],
			    [ 'name' => 'Expiración de contrato'],
			    [ 'name' => 'Rescisión de contrato'],
                [ 'name' => 'Despido'],
                [ 'name' => 'Defunción']
		    ]
	    );
    }
}
