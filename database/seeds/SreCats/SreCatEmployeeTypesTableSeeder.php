<?php

use Illuminate\Database\Seeder;

class SreCatEmployeeTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sre_cat_employee_types')->insert(
	    	[
			    [ 'name' => 'Empleado de la Cancillería' ],
			    [ 'name' => 'Empleado externo' ]
		    ]
	    );
    }
}
