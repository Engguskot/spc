<?php

use Illuminate\Database\Seeder;

class SreCatEmployeeGendersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('sre_cat_employee_genders')->insert(
	    	[
			    ['id' => 1, 'name' => 'Masculino'],
			    ['id' => 2, 'name' => 'Femenino']
		    ]
	    );
    }
}
