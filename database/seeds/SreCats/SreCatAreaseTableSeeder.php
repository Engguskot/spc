<?php

use Illuminate\Database\Seeder;

class SreCatAreasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        
        \DB::table('sre_cat_areas')->insert(array (
            
            array (
                'id' => 1,
                'name' => 'Oficinas del C. Secretario'
            ),
            
            array (
                'id' => 2,
                'name' => 'Subsecretaría de Relaciones Exteriores'
            ),
            
            array (
                'id' => 3,
                'name' => 'Subsecretaría para América del Norte'
            ),
            
            array (
                'id' => 4,
                'name' => 'Subsecretaría para América Latina y el Caribe'
            ),
            
            array (
                'id' => 5,
                'name' => 'Subsecretaría para Asuntos Multilaterales y Derechos Humanos'
            ),
            
            array (
                'id' => 6,
                'name' => 'Oficialía Mayor'
            ),
            
            array (
                'id' => 7,
                'name' => 'Agencia Mexicana de Cooperación Internacional para el Desarrollo'
            ),
            
            array (
                'id' => 8,
                'name' => 'Consultoría Jurídica'
            ),
            
            array (
                'id' => 9,
                'name' => 'Órgano Interno de Control'
            ),
            
            array (
                'id' => 10,
                'name' => 'Sindicato Nacional de Trabajadores'
            ),
        ));
        
        
    }
}