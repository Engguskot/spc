<?php

use Illuminate\Database\Seeder;

class SreCatSubAreasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        
        \DB::table('sre_cat_sub_areas')->insert(array (
            
            array (
                'id' => 1,
                'name' => 'Jefe de Oficina',
                'sre_cat_area_id' => 1,
                'created_at' => '2018-01-14 05:40:32',
                'updated_at' => '2018-01-14 05:40:32'
            ),
            
            array (
                'id' => 2,
                'name' => 'Secretaría Particular',
                'sre_cat_area_id' => 1,
                'created_at' => '2018-01-14 05:40:32',
                'updated_at' => '2018-01-14 05:40:32'
            ),
            
            array (
                'id' => 3,
                'name' => 'Coordinación Administrativa',
                'sre_cat_area_id' => 1,
                'created_at' => '2018-01-14 05:40:32',
                'updated_at' => '2018-01-14 05:40:32'
            ),
            
            array (
                'id' => 4,
                'name' => 'Secretaría Particular',
                'sre_cat_area_id' => 2,
                'created_at' => '2018-01-14 05:40:32',
                'updated_at' => '2018-01-14 05:40:32'
            ),
            
            array (
                'id' => 5,
                'name' => 'Coordinación de Asesores',
                'sre_cat_area_id' => 2,
                'created_at' => '2018-01-14 05:40:32',
                'updated_at' => '2018-01-14 05:40:32'
            ),
            
            array (
                'id' => 6,
                'name' => 'Coordinación Administrativa',
                'sre_cat_area_id' => 2,
                'created_at' => '2018-01-14 05:40:32',
                'updated_at' => '2018-01-14 05:40:32'
            ),
            
            array (
                'id' => 7,
                'name' => 'Secretaría Particular',
                'sre_cat_area_id' => 3,
                'created_at' => '2018-01-14 05:40:32',
                'updated_at' => '2018-01-14 05:40:32'
            ),
            
            array (
                'id' => 8,
                'name' => 'Coordinación de Asesores',
                'sre_cat_area_id' => 3,
                'created_at' => '2018-01-14 05:40:32',
                'updated_at' => '2018-01-14 05:40:32'
            ),
            
            array (
                'id' => 9,
                'name' => 'Coordinación Administrativa',
                'sre_cat_area_id' => 3,
                'created_at' => '2018-01-14 05:40:32',
                'updated_at' => '2018-01-14 05:40:32'
            ),
            
            array (
                'id' => 10,
                'name' => 'Secretaría Particular',
                'sre_cat_area_id' => 4,
                'created_at' => '2018-01-14 05:40:33',
                'updated_at' => '2018-01-14 05:40:33'
            ),
            
            array (
                'id' => 11,
                'name' => 'Coordinación de Asesores',
                'sre_cat_area_id' => 4,
                'created_at' => '2018-01-14 05:40:33',
                'updated_at' => '2018-01-14 05:40:33'
            ),
            
            array (
                'id' => 12,
                'name' => 'Coordinación Administrativa',
                'sre_cat_area_id' => 4,
                'created_at' => '2018-01-14 05:40:33',
                'updated_at' => '2018-01-14 05:40:33'
            ),
            
            array (
                'id' => 13,
                'name' => 'Secretaría Particular',
                'sre_cat_area_id' => 5,
                'created_at' => '2018-01-14 05:40:33',
                'updated_at' => '2018-01-14 05:40:33'
            ),
            
            array (
                'id' => 14,
                'name' => 'Coordinación de Asesores',
                'sre_cat_area_id' => 5,
                'created_at' => '2018-01-14 05:40:33',
                'updated_at' => '2018-01-14 05:40:33'
            ),
            
            array (
                'id' => 15,
                'name' => 'Coordinación Administrativa',
                'sre_cat_area_id' => 5,
                'created_at' => '2018-01-14 05:40:33',
                'updated_at' => '2018-01-14 05:40:33'
            ),
            
            array (
                'id' => 16,
                'name' => 'Coordinación de Asesores',
                'sre_cat_area_id' => 6,
                'created_at' => '2018-01-14 05:40:33',
                'updated_at' => '2018-01-14 05:40:33'
            ),
            
            array (
                'id' => 17,
                'name' => 'Coordinación Logística',
                'sre_cat_area_id' => 6,
                'created_at' => '2018-01-14 05:40:33',
                'updated_at' => '2018-01-14 05:40:33'
            ),
            
            array (
                'id' => 18,
                'name' => 'Coordinación Administrativa',
                'sre_cat_area_id' => 6,
                'created_at' => '2018-01-14 05:40:33',
                'updated_at' => '2018-01-14 05:40:33'
            ),
            
            array (
                'id' => 19,
                'name' => 'Coordinación de Asesores',
                'sre_cat_area_id' => 7,
                'created_at' => '2018-01-14 05:40:33',
                'updated_at' => '2018-01-14 05:40:33'
            ),
            
            array (
                'id' => 20,
                'name' => 'Coordinación Administrativa',
                'sre_cat_area_id' => 7,
                'created_at' => '2018-01-14 05:40:33',
                'updated_at' => '2018-01-14 05:40:33'
            ),
            
            array (
                'id' => 21,
                'name' => 'Consultoría Jurídica Adjunta A',
                'sre_cat_area_id' => 8,
                'created_at' => '2018-01-14 05:40:33',
                'updated_at' => '2018-01-14 05:40:33'
            ),
            
            array (
                'id' => 22,
                'name' => 'Consultoría Jurídica Adjunta B',
                'sre_cat_area_id' => 8,
                'created_at' => '2018-01-14 05:40:33',
                'updated_at' => '2018-01-14 05:40:33'
            ),
        ));
        
        
    }
}