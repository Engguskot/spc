<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// App 
Route::get('/', function() {
	return view('app');
});

// Login, logout and session user information
Route::post('/login',		'Auth\LoginController@login')->name('login');
Route::post('/logout',		'Auth\LoginController@logout')->name('logout');
Route::get('/user/{id}',	'Auth\LoginController@user')->name('user');

// Keep alive
Route::any('{path?}', function() {
	return view("app");
})->where("path", ".+");






