<?php

namespace App\Http\Models\Sre;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Models\Geo\GeoCatIdiom;

class SreEmployeeIdiom extends Model
{
	use SoftDeletes;

	protected $fillable = [ 'sre_employee_id', 'geo_cat_idioms_id' ];

	function idioms()
	{
		return $this->belongsTo(GeoCatIdiom::class, 'geo_cat_idioms_id');
	}
}
