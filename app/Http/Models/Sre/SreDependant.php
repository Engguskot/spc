<?php

namespace App\Http\Models\Sre;

use Illuminate\Database\Eloquent\Model;
use App\Http\Models\Sre\SreDependantDocument;

class SreDependant extends Model
{

	function documents()
	{
			return $this->hasMany(SreDependantDocument::class);
	}
}
