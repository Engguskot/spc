<?php

namespace App\Http\Models\Sre;

use Illuminate\Database\Eloquent\Model;
use App\Http\Models\Sre\SreJobBenefit;
use App\Http\Models\SreCats\SreCatOffice;
use App\Http\Models\SreCats\SreCatFunctionType;

class SreJob extends Model
{

	protected $fillable = [
							'name',
							'sre_cat_status_job_id',
							'sre_cat_office_id',
							'sre_cat_department_id',
							'sre_cat_function_type_id'
						   ];

	function benefits()
	{
		return $this->hasMany(SreJobBenefit::class);
	}

	public function office()
	{
		return $this->belongsTo(SreCatOffice::class, 'sre_cat_office_id');
	}

	public function functionType()
	{
		return $this->belongsTo(SreCatFunctionType::class, 'sre_cat_function_type_id');
	}

}
