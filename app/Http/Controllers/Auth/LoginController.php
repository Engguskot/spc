<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use Auth;

class LoginController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles authenticating users for the application and
	| redirecting them to your home screen. The controller uses a trait
	| to conveniently provide its functionality to your applications.
	|
	*/

	use AuthenticatesUsers;

	public function login( Request $credentials ){
		$authenticated	= false;
		$token			= '';
		$expiration		= 0;
		$user 			= (object)[];
		$credentials	= $credentials->all();
		$keepSession	= $credentials['keepSession'];
		$authenticate	= [
			'username'	=> $credentials['username'],
			'password'	=> $credentials['password']
		];

		if ( Auth::attempt($authenticate, $keepSession) ) {
			$authenticated	= true;
			$user			= Auth::user();
			$token			= $user->createToken('gmm_token')->accessToken;
			$expiration		= strtotime('1 day', 0);
			// Generate user session info
			$user = static::getSessionInfo($user->id);
		}

		return response()->json([
			'authenticated'			=> $authenticated,
			'gmm_token'				=> $token,
			'gmm_token_expiration'	=> $expiration,
			'gmm_keep_session'		=> $keepSession,
			'gmm_user'				=> encrypt(Auth::user()->id),
			'user'					=> $user
		], 200);
	}

	public function logout(){
		$authenticated = true;
		Auth::logout();

		if ( !Auth::check() ) { $authenticated = false; }

		return response()->json([
			'authenticated' => $authenticated
		], 200);
	}

	public function user( Request $request, $id ){
		$id = decrypt($id);
		$user = (object)[];
		$user = static::getSessionInfo($id);

		return response()->json([
			'user' => $user
		], 200);
	}

	public static function getSessionInfo($id){
		$user		= User::find($id);
		return $user;
	}

}
